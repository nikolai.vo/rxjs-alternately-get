import { Observable } from 'rxjs';
import { tap, map, switchMap, take, scan, observeOn, throttleTime, flatMap, filter, pluck, debounceTime, merge } from 'rxjs/operators';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { pipe } from 'rxjs/Rx';
import { User, Devices, Apps } from './models';
import { AjaxResponse } from 'rxjs/observable/dom/AjaxObservable';
import { Ajax } from './ajax';


const API_URL = 'https://rx-microadmin.herokuapp.com';
const request = new Ajax();

request.get(`${API_URL}/users`)
  .pipe(
    switchMap((users: Array<User>): Observable<Array<User>> => {
      // получаем новый массив урлов юзеров(с ограничением 2 шт)
      const devices$ = [].concat(users).slice(0, 2).map(user => {

        const devicesUrl = `${API_URL}/devices?user_id=${user.id}`;
        // получаем devices юзеров
        return request.get(devicesUrl)
          .pipe(
            map((devices: Array<Devices>): User => {
              // мержим в юзера все его девайсы
              return {
                ...user,
                devices
              }
            })
          );
      });

      // запускаем девайсы юзеров одновременно
      return forkJoin(devices$);
    }),
    switchMap((users: Array<User>): Observable<Array<User>> => {
      const appsItems$ = users
        .map((user: any) => {
          // для девайсов каждого юзера выбираем их apps
          const userApps$ = user.devices.map(device => {
            const appsItem = `${API_URL}/apps?host_id=${device.id}`;

            return request.get(appsItem)
              .pipe(
                map((apps: Array<Apps>): Devices => {
                  // ращитываем в процентах загруженность девайса
                  const total = apps.reduce((accum, item) => accum + item.size, 0);
                  const loadPercent = Math.round(total / device.capacity * 100) + ' ' + '%';

                  return {
                    ...device,
                    apps,
                    loadPercent
                  };
                })
              );
          });
          // запускаем запросы деайсов на  аpps одновременно
          return forkJoin(userApps$).pipe(
            map(devices => {
              return {
                ...user,
                devices
              }
            })
          );
        });

      return forkJoin(appsItems$);
    }),
    tap(users => console.log('[Result]', users))
  )
  .subscribe()

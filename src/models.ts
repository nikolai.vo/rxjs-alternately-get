export interface User {
  id: string;
  name: string;
  email: string;
  devices?: Array<Devices>;
 
}

export interface Devices {
  id: string;
  user_id: string;
  description: string;
  ip: string;
  capacity: number;
  apps?: Array<Apps>;
  loadPercent?: string;
}

export interface Apps {
  id: string
  title: string
  size: number
  host_id: string
 
}

